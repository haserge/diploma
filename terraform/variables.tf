variable "cloud_password" {
  sensitive   = true
  description = "VK cloud password"
}

variable "main_db_user_password" {
  sensitive   = true
  description = "main DB password"
}

variable "develop_db_user_password" {
  sensitive   = true
  description = "develop DB password"
}

variable "main_db_username" {
  sensitive   = true
  description = "main DB username"
}

variable "develop_db_username" {
  sensitive   = true
  description = "develop DB username"
}

variable "main_db_name" {
  description = "main DB name"
}

variable "develop_db_name" {
  description = "develop DB name"
}

variable "public-key-file" {
  type    = string
  default = "c:\\Users\\user\\.ssh\\id_rsa.pub"
}

variable "db-instance-flavor" {
  type    = string
  default = "Basic-1-2-20"
}
