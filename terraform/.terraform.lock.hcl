# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/mailrucloudsolutions/mcs" {
  version     = "0.6.14"
  constraints = "0.6.14"
  hashes = [
    "h1:Yj0LruhAKNUCVbs2Tnm0nklkr9utgmHy//8oLNNGcx0=",
    "zh:07ec645031622b0fa25e12e6c2c258315aeda75076472152699809b9a7845c67",
    "zh:20613fd9eb1860160e76ca6802b84d0de1a971fe96265acc3dd61fd043e4d41d",
    "zh:3de6e9e52b0566a12ba41825a85f75d04668a0622bd586d826e8bf766e29311c",
    "zh:410577d41e1a09cab078e76b1adb6e5fc3d99e8b6df51db7d7e07949d2c9d531",
    "zh:42906d0ef89a3f08842686824aa9f3e73413da5128e03afeb82a19c49af7c98d",
    "zh:42de74dfc4862cea08d1c018be980e74057c96820ed398eb5ace4b65652704a6",
    "zh:4c5752be47c64fed993156bc89f4ea3e2e32c1bbeee4060984f1b8fa5ae1b471",
    "zh:55da94d1d97f4fc9f8dd3ba2f3c2af7a8aeb360168766e217c691932c82cb614",
    "zh:5e74bef74304c92406d320bed7882df5d22672209b61c5ff237c4a898bfce2e1",
    "zh:67684beec6065f3ea7d00a2e69f21784e3e5e119d6ad99c9f22b58570eb01e93",
    "zh:911de16ebfae37379f0be48605e5f8cba93bfc41b44a4c63c2e76be7354e4ff9",
    "zh:9b3d39bd117b3333784f358cdc0369a2a9e2ecfcc40ae40dd67ec6240ba717be",
    "zh:a144ee2258a0c139124a4a9f4eadaa3e7d8f71b4adf86e262ad6f2e1765b910e",
    "zh:bef277f1e5f42942220299c7465038167553b63a931b2e926b7f94d8dc541d23",
  ]
}

provider "registry.terraform.io/terraform-provider-openstack/openstack" {
  version     = "1.47.0"
  constraints = "1.47.0"
  hashes = [
    "h1:2+Unmh+fE0u8xukBCmv/mUHcWrLBiFouhiqcHP/ZG6s=",
    "zh:110dfeb02d47af0d1c71d41deac7bb0b23d0d5f9f2ac95f81fee7be1b7131852",
    "zh:164c141ca8d1d1b43b866150797f5e15855d48aaddd50a80ca320e638cdbbd3a",
    "zh:1be4fab5de93f2947c35df6676c67bf2ea410ec71f29e4a57661119c2f262d6a",
    "zh:40dc8f1ffc3521786b38427fe2c0f2ec0b102fed4284c08b2d092ebd1617a603",
    "zh:6335190faaeba5bd5a859df7075c6a820a1f492d1f2258296a4fb3170b4f0643",
    "zh:6460c8c651ba96d434e0db0e09cb02b6188861a8a683b1f9a488d0e119deeb71",
    "zh:6ba87e36384c8c165e0853366252451a20a2c1aeec1a2280eb208db200b5ce33",
    "zh:6ebbb699c8673ebbaa5fc880c7b3a8dcf69d51bf242c72b986cf6b919bac4969",
    "zh:719dc962699d17d03f017d27809c68162a2a849e478e04b858cd798be737a26a",
    "zh:7d865928a695dc3558f671da12dbb134d822968d2f0cb4b600ce79c41a0d1d4b",
    "zh:b5c50a6ab05cb18780f4d4b4d48f502d60f4121810127105f5f7d34d2dda9569",
    "zh:df3adcaacfdd3696a9df768beb3ec997410724c5dbd1cc5fd023e6644686bd74",
    "zh:f836c285e5e36ddaa3d0d13f4e89fcb9047356cfb09c77651cdf97b6394af7fa",
    "zh:f91081a4f26b69b8325149353d3199838466e971853a2d1e9c2917051a6fdd6d",
  ]
}
