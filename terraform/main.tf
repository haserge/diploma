terraform {
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "1.47.0"
    }
    mcs = {
      source  = "MailRuCloudSolutions/mcs"
      version = "0.6.14"
    }
  }
}

provider "openstack" {
  # Your user account.
  user_name = "drbykow@gmail.com"

  # The password of the account
  password = var.cloud_password

  # The tenant token can be taken from the project Settings tab - > API keys.
  # Project ID will be our token.
  tenant_id = "30d8836325754474881264bc236639b0"

  # The indicator of the location of users.
  user_domain_id = "users"

  # API endpoint
  # Terraform will use this address to access the VK Cloud Solutions api.
  auth_url = "https://infra.mail.ru:35357/v3/"

  # use octavia to manage load balancers
  use_octavia = true

  # Region name
  region = "RegionOne"
}

provider "mcs" {
  # Your user account.
  username = "drbykow@gmail.com"

  # The password of the account
  password = var.cloud_password

  # The tenant token can be taken from the project Settings tab - > API keys.
  # Project ID will be our token.
  project_id = "30d8836325754474881264bc236639b0"

  # Region name
  region = "RegionOne"
}
