data "openstack_compute_flavor_v2" "db" {
  name = var.db-instance-flavor
}

resource "openstack_compute_keypair_v2" "keypair" {
  name       = "default_key"
  public_key = file(var.public-key-file)
}

resource "openstack_networking_network_v2" "k8s" {
  name           = "k8s-net"
  admin_state_up = true
}

resource "openstack_networking_subnet_v2" "k8s-subnetwork" {
  name            = "k8s-subnet"
  network_id      = openstack_networking_network_v2.k8s.id
  cidr            = "10.110.0.0/16"
  ip_version      = 4
  dns_nameservers = ["8.8.8.8", "8.8.4.4"]
}

data "openstack_networking_network_v2" "extnet" {
  name = "ext-net"
}

resource "openstack_networking_router_v2" "k8s" {
  name                = "k8s-router"
  admin_state_up      = true
  external_network_id = data.openstack_networking_network_v2.extnet.id
}

resource "openstack_networking_router_interface_v2" "k8s" {
  router_id = openstack_networking_router_v2.k8s.id
  subnet_id = openstack_networking_subnet_v2.k8s-subnetwork.id
}

data "openstack_compute_flavor_v2" "k8s_master" {
  name = "Standard-2-4-40"
}

data "openstack_compute_flavor_v2" "k8s_worker" {
  # name = "Standard-2-4-40"
  name = "Standard-4-12"
}

data "mcs_kubernetes_clustertemplate" "cluster_template" {
  version = "1.22.6"
}

resource "mcs_kubernetes_cluster" "k8s-cluster" {
  depends_on          = [openstack_networking_router_interface_v2.k8s, ]
  name                = "k8s-cluster"
  cluster_template_id = data.mcs_kubernetes_clustertemplate.cluster_template.id
  master_flavor       = data.openstack_compute_flavor_v2.k8s_master.id
  master_count        = 1
  availability_zone   = "MS1"
  labels = {
    ingress_controller = "nginx"
  }
  keypair             = openstack_compute_keypair_v2.keypair.id
  network_id          = openstack_networking_network_v2.k8s.id
  subnet_id           = openstack_networking_subnet_v2.k8s-subnetwork.id
  floating_ip_enabled = true
}

resource "mcs_kubernetes_node_group" "default_ng" {
  depends_on          = [mcs_kubernetes_cluster.k8s-cluster, ]
  cluster_id          = mcs_kubernetes_cluster.k8s-cluster.id
  name                = "default"
  node_count          = 1
  autoscaling_enabled = false
  max_nodes           = 2
  min_nodes           = 1
  flavor_id           = data.openstack_compute_flavor_v2.k8s_worker.id
}

resource "mcs_db_instance" "db-instance" {
  # depends_on = [mcs_kubernetes_node_group.default_ng, ]
  name = "db-instance"

  datastore {
    type    = "postgresql"
    version = "12"
  }
  keypair     = openstack_compute_keypair_v2.keypair.id
  flavor_id   = data.openstack_compute_flavor_v2.db.id
  size        = 8
  volume_type = "ceph-ssd"
  disk_autoexpand {
    autoexpand    = true
    max_disk_size = 1000
  }

  network {
    uuid = openstack_networking_network_v2.k8s.id
  }
}

resource "mcs_db_database" "main_db_database" {
  name    = var.main_db_name
  dbms_id = mcs_db_instance.db-instance.id
  charset = "utf8"
  # collate = "utf8_general_ci"
}

resource "mcs_db_user" "main_db_user" {
  name      = var.main_db_username
  password  = var.main_db_user_password
  dbms_id   = mcs_db_instance.db-instance.id
  databases = [mcs_db_database.main_db_database.name]
}

resource "mcs_db_database" "develop_db_database" {
  name    = var.develop_db_name
  dbms_id = mcs_db_instance.db-instance.id
  charset = "utf8"
}

resource "mcs_db_user" "develop_db_user" {
  name      = var.develop_db_username
  password  = var.develop_db_user_password
  dbms_id   = mcs_db_instance.db-instance.id
  databases = [mcs_db_database.develop_db_database.name]
}
