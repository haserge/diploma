import migrate
import traceback
import urllib3
import xmltodict
from datetime import datetime
from flask import Flask, jsonify

month = datetime.now().month
year = datetime.now().year
format1 = '%d%m%Y'
format2 = '%d/%m/%Y'
format3 = '%Y-%m-%d'
format4 = '%d.%m.%Y'
name_table = migrate.name_table
run_date = 1
data_for_update = []


# getting data about daily currency exchange rates in xml format from api
def get_xml_data(dd):
    request_date = datetime.strptime(str(dd) + str(month) + str(year), format1).strftime(format2)
    url = f'https://www.cbr.ru/scripts/XML_daily.asp?date_req={request_date}'
    http = urllib3.PoolManager()
    response = http.request('GET', url)
    try:
        data = xmltodict.parse(response.data)
        data['request_date'] = datetime.strptime(str(dd) + str(month) + str(year), format1).strftime(format4)
    except:
        print('Failed to parse xml from response (%s)' % traceback.format_exc())
    return data


# parse xml data and insert it into database table
def prepare_data(raw_data, rows_exist_in_db, save_row_for_update):
    cursor = migrate.create_con_cur()
    elem1 = raw_data['ValCurs']['@Date']
    elem2 = raw_data['ValCurs']['Valute']
    elem3 = raw_data['request_date']
    for i in range(0, len(elem2)):
        date_valute_id = datetime.strptime(elem3, format4).strftime(format3) + elem2[i]['@ID']
        if date_valute_id not in rows_exist_in_db:
            request_date = elem3
            col_date = elem1
            col_valute_id = elem2[i]['@ID']
            col_numcode = elem2[i]['NumCode']
            col_charcode = elem2[i]['CharCode']
            col_nominal = elem2[i]['Nominal']
            col_name = elem2[i]['Name']
            col_value = elem2[i]['Value']
            cursor.execute("insert into " + name_table + " values (%s, %s, %s, %s, %s, %s, %s, %s)",
                           (request_date, col_date, col_valute_id, col_numcode, col_charcode, col_nominal, col_name,
                            col_value))
            if save_row_for_update:
                new_row = [request_date, col_date, col_valute_id, col_numcode, col_charcode, col_nominal, col_name,
                           col_value]
                # rows added during last update (after clicking the Data update button)
                data_for_update.append(new_row)
    cursor.close()


# load data by days from api and check if they are in the database
def store_data_into_db(first_date, from_update):
    global run_date
    print("Start update data from day number:", run_date)
    for day in range(first_date, datetime.now().day + 1):
        check_date = (str(day) + '.' + str(month) + '.' + str(year))
        rows_exist_in_db = data_from_db(check_date, 'concat(request_date, col_valute_id)', 'data_check')
        if not rows_exist_in_db or day >= run_date:
            print("Load data from api https://www.cbr.ru/development/SXML/. Day:", day)
            prepare_data(get_xml_data(day), rows_exist_in_db, from_update)
    run_date = datetime.now().day
    print("Next time the data will be updated from day number:", run_date)


# update data (after clicking the Data update button)
def db_data_update():
    global run_date
    data_for_update.clear()
    if datetime.now().day < run_date:
        run_date = 1
    store_data_into_db(run_date, True)


# get data from database
def data_from_db(request_date, db_fields, who_request):
    cursor = migrate.create_con_cur()
    cursor.execute("select " + db_fields + " from " + name_table + " where request_date = '" + request_date + "' order by col_name")
    rows = cursor.fetchall()
    ids_for_check = []
    rows_for_html = []
    output_data = []
    # form a list for later comparison
    if who_request == "data_check":
        for row in rows:
            ids_for_check.append(row[0])
        output_data = ids_for_check
    # create a list to display on a web page
    elif who_request == "show_data":
        for row in rows:
            row_for_html = list(row)
            row_for_html[0] = datetime.strftime(row[0], format4)
            rows_for_html.append(row_for_html)
        output_data = rows_for_html
    cursor.close()
    return output_data


# delete the latest currency exchange rate for today from the database (for testing purposes)
def delete_row():
    cursor = migrate.create_con_cur()
    request_date = (str(year) + '-' + str(month) + '-' + str(datetime.now().day))
    cursor.execute("select col_name from " + name_table + " where request_date = '" + request_date + "' order by col_name desc")
    max_name = cursor.fetchone()
    if max_name is None:
        message = "There are no more rows in database for current day"
    else:
        cursor.execute(
            "select * from " + name_table + " where request_date = '" + request_date + "' and col_name = '" + max_name[0] + "'")
        deleted_row = cursor.fetchall()
        cursor.execute("delete from " + name_table + " where request_date = '" + request_date + "' and col_name = '" + max_name[0] + "'")
        message = 'Row -- ' + datetime.strftime(deleted_row[0][0], format4) + ' ' + deleted_row[0][1] + ' ' + \
                  deleted_row[0][2] + ' ' + str(deleted_row[0][3]) + ' ' + deleted_row[0][4] + ' ' + str(deleted_row[0][5]) + \
                  ' ' + deleted_row[0][6] + ' ' + deleted_row[0][7] + ' -- deleted from the database.'
        cursor.close()
    return message


app = Flask(__name__)
print("Backend v2.3 is ready")
load_start_time = datetime.now()
store_data_into_db(run_date, False)
load_end_time = datetime.now()
print("Data received for: ", load_end_time - load_start_time, " sec.")


@app.route("/get_data/<set_date>", methods=["GET"])
def start_page(set_date):
    data = data_from_db(set_date, "*", "show_data")
    return jsonify(data)


@app.route("/update", methods=["GET"])
def update():
    db_data_update()
    return jsonify(data_for_update)


@app.route("/delete", methods=["GET"])
def delete():
    info = delete_row()
    return jsonify(info)


# if __name__ == "__main__":
#    app.run(host='0.0.0.0')