### Prometheus stack

```sh
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm repo update
```
1. Create namespace for Prometheus and Grafana `kubectl apply -f namespace-kube-graph.yaml`
2. Run installation `helm install kube-prometheus-stack prometheus-community/kube-prometheus-stack --namespace kube-graph`
3. Expose port for prometheus web UI (optional):
```sh
kubectl port-forward -n kube-graph prometheus-kube-prometheus-stack-prometheus-0 --address 0.0.0.0 9090
```
4. Create ingress for Grafana `kubectl apply -f ingress_grafana.yaml`
5.  Grafana web UI: `http://graf.aokcbk.ru`

### Elastic stack

1. Install custom resource definitions: `kubectl create -f https://download.elastic.co/downloads/eck/2.2.0/crds.yaml`
2. Install the operator with its RBAC rules: `kubectl apply -f https://download.elastic.co/downloads/eck/2.2.0/operator.yaml`
3. Deploy Elastic: `kubectl apply -f deploy_elastic.yaml`
4. Deploy Kibana: `kubectl apply -f deploy_kibana.yaml`
5. Deploy Filebeat: `kubectl apply -f filebeat_autodiscover.yaml`
6. Get the credentials. A default user named elastic is automatically created with the password stored in a Kubernetes secret:
```sh
kubectl get secret elasticsearch-es-elastic-user -o go-template='{{.data.elastic | base64decode}}'
```
7. Expose port for elastic (optional):
```sh
kubectl port-forward service/elasticsearch-es-http 9200
```
8. Get the Kibana web UI address:
```sh
kubectl get svc kibana-kb-http
kibana-kb-http   LoadBalancer   10.254.81.121   37.139.40.154   5601:30761/TCP   2d6h

Open web URL:
http://37.139.40.154:5601
```
